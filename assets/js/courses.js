let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")
let cardFooter;
let activation;
let userId = localStorage.getItem("id")
// console.log(userId)

if (adminUser === false || !adminUser) {

	addButton.innerHTML = `<a href="#coursesContainer" class="btn courseBtn"> CHECK LEARNING TENTS </a>
	`

	fetch('https://aqueous-garden-97949.herokuapp.com/api/courses/active')
	.then(res => res.json())
	.then(data => {
		// console.log(data);
		// a variable that stores the data to be rendered
		let courseData;
		//if number of courses < 1, display  no courses available
		if (data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map( course => {
				// console.log(course);
			
				if (!userId) {
					// console.log("not user")
					cardFooter = 
						`
						<a href="./register.html?courseId=${course._id}" value={course._id} class="btn courseBtn">
								Select Course 
						</a>
						`
				}
				else {
					// console.log("user")
					cardFooter = 
						`
							<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-default courseBtn">
								Select Course 
							</a>
						`
				}
				
				return (
						`
							<div class="col-md-6 col-lg-4 my-3">
							<div class="card">
								<img class="card-img-top rounded-circle" src="../assets/images/tent.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Tent: ${course.name}</h5>
									<p class="card-text text-left">
										${course.description}
									</p>
									<p class="card-text text-right">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer">
									<div class="mb-2">${cardFooter}</div>
								</div>
							</div>
						</div>
						`
					)

			// since the collection of courseData is an array, .join method indicates a separator for each element
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData;

	})
}

else {

	// console.log("hello admin")
	
	addButton.innerHTML =  `<a href="./addCourse.html" class="btn courseBtn"> ADD A COURSE </a>
	`
	fetch('https://aqueous-garden-97949.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		let courseData;
		//if number of courses < 1, display  no courses available
		if (data.length < 1){
			courseData = "No courses available"
		} else {
			// console.log(data)
			courseData = data.map( course => {
				// console.log(course);

				cardFooter = 
					`
						<a href="./viewCourse.html?courseId=${course._id}" value={course._id} class="btn infoButton courseBtn">
							View
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn editButton courseBtn">
							Edit
						</a>
					`
				if (course.isActive === true)
					{
						activation = 
							`<a href="./deleteCourse.html?courseId=${course._id}" value={course._id}  id="deleteButton" class="btn courseBtn">
								Archive
							</a>
						`

						return (
							`
							<div class="col-md-6 col-lg-4 my-3">
								<div class="card">
									<img class="card-img-top rounded-circle" src="../assets/images/tent.png" alt="Card image cap">
									<div class="card-body">
										<h5 class="card-title">Tent: ${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer">
										<div class="row">
											<div class="col-auto mr-auto">
												${cardFooter}
											</div>
											<div class="col-auto">
												${activation}
											</div>
										</div>
									</div>
								</div>
							</div>
							`
						)

					}
				else {
					{
						activation = 
							`<a href="./reactivateCourse.html?courseId=${course._id}" value={course._id} id="activateButton" class="btn courseBtn">
								Re-Activate
							</a>
						`
						return (
							`
								<div class="col-md-6 col-lg-4 my-3">
									<div id="archived" class="card">
										<img class="card-img-top rounded-circle" src="../assets/images/tent.png" alt="Card image cap">
										<div class="card-body">
											<h5 class="card-title">Tent: ${course.name}</h5>
											<p class="card-text text-left">
												${course.description}
											</p>
											<p class="card-text text-right">
												₱ ${course.price}
											</p>
										</div>
										<div class="card-footer">
											<div class="row">
												<div class="col-auto mr-auto">
													${cardFooter}
												</div>
												<div class="col-auto">
													${activation}
												</div>
											</div>
										</div>
									</div>
								</div>
							`
						)

					}
				}

			}).join("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData;

	})
}



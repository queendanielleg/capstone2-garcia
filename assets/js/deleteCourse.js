// console.log("hello")

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token")

fetch(`https://aqueous-garden-97949.herokuapp.com/api/courses/${courseId}`, {
	method: "DELETE",
	headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data => {
	if(data === true){
		alert('Course has been archived')
		// console.log(true)
		// window.location.replace('./courses.html')
	} else {
		alert("Something went wrong")
	}
})
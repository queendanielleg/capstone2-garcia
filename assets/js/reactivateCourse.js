// console.log("hello from reactivateCourse")


let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
// console.log(courseId)

let token = localStorage.getItem("token")
// console.log(token)

fetch(`https://aqueous-garden-97949.herokuapp.com/api/courses/${courseId}`, {
	method: "PUT",
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then( data => {
	// console.log(data)
	if (data === true) {
		alert("Course is successfully reactivated")
		// window.location.replace("./courses.html")
		// console.log(true)
	}
	else {
		alert("Something went wrong")
	}
})


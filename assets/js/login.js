let loginForm = document.querySelector("#logInUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	// console.log(email)
	// console.log(password)

	if (email === "" || password === "") {
		alert("Please input your email and/or password")
	} else {
		fetch('https://aqueous-garden-97949.herokuapp.com/api/users/login', {
			method: "POST",
			headers: { "Content-Type" : "application/json" },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			
			//if else statement for verification of successful login and access token
			// if failed login, they will get an alert. if successful login, we will save token into web browser thru accessing the local storage and get his details using fetch
			//local storage is read only; therefore, we set our data into the localStorage through javaScript by providing a key/value pair into the storage
			//method setItem() for localStorage allows too save data into localStorage. 
			//This is the syntax: localStorage.setItem("key", data)

			//to get an item from localStorage
			//this is the syntax: localStorage.getItem("key")

			if (data.accessToken) {
				//set the token into the local storage
				localStorage.setItem('token', data.accessToken)

				//send a fetch request to decode the JWT and obtain the user ID and isAdmin property
				//NOTE: for GET requests, method defined in options is not needed

				fetch('https://aqueous-garden-97949.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					localStorage.setItem('id',data._id)
					localStorage.setItem('isAdmin', data.isAdmin)

					//window.location.replace = redirect user to another page
					window.location.replace("./courses.html")
				})

			} else {
				alert("Login Failed. Something went wrong.")
			}
		})
	}
})
// console.log("hello")

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")
let admin = localStorage.getItem("isAdmin")
// console.log(admin)

let userName = document.querySelector("#userName")
let mobileNumber = document.querySelector("#mobileNumber")
let email = document.querySelector("#email")
let courses = document.querySelector("#courses")

let heading1 = document.querySelector("#heading1")
let tentsContainer = document.querySelector("#tentsContainer")
let enrollmentBelow = document.querySelector("#enrollmentBelow")
let redirectTents = document.querySelector("#redirectTents")

// console.log(userName.value)
// console.log(mobileNumber.value)
// console.log(courses.value)
let editProfile = document.querySelector("#editProfile")			
editProfile.innerHTML =`<a href="./editProfile.html?userId=${userId}" value={course._id} class="btn btn-block courseBtn">
		EDIT MY PROFILE
	</a>` 

let userCourses = []

fetch(`https://aqueous-garden-97949.herokuapp.com/api/users/details`, {
	headers: {
			'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then( data => {
	// console.log(data)

	userName.innerHTML = `${data.firstName} ${data.lastName}`
	email.innerHTML = data.email
	mobileNumber.innerHTML = data.mobileNo

	// let courseId = data.enrollments[0].courseId
	// console.log(courseId)
	let courseId = [];

	if (admin === "false") {

		document.getElementById("tentsContainer").classList.add("mb-5");
		heading1.innerHTML = "GOOD DAY, CAMPER!"
		enrollmentBelow.innerHTML = "LIST OF YOUR ENROLLMENTS BELOW"
		redirectTents.innerHTML = `<a href="#tentsContainer" class="btn btn-block courseBtn">CHECK YOUR TENTS
			</a>`

		courseId = data.enrollments.map( course => {

			let enrolledCourses = course.courseId
			// console.log(enrolledCourses)
			// getCourseId = course.courseId
			// console.log(getCourseId)
			const courseName = fetch(`https://aqueous-garden-97949.herokuapp.com/api/courses/${enrolledCourses}`)
			.then(res => res.json())
			.then( course => {
				// console.log(course)
				return course.name
			})

			// console.log(courseName)
			
			const printName = () => {
				courseName.then((name)=> {
					userCourses.push(name)
					userCourses.sort()
					// console.log(userCourses)
				})
				.then( data => { 
					let list = userCourses.map(data => {
						return (`
							<div class="col-md-5 forTents my-3 text-center"> <img src="./../assets/images/tent.png" alt="Tent" class="img-fluid rounded-circle"> <br> Tent Name: ${data}</div>`)
					}).join("")
					courses.innerHTML = list;
				})
			}
			printName();
		})
	}
	else {

		heading1.innerHTML = "GOOD DAY, ADMIN!"
		tentsContainer.innerHTML = null
		enrollmentBelow.innerHTML = "OF COURSE, NO ENROLLMENTS :)"
		redirectTents.innerHTML = null
	} 
})


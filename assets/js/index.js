let navItems = document.querySelector("#navSession");
let registerLink = document.querySelector("#register");
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin") === "true";

if (!userToken) {
	navItems.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./pages/login.html" class="nav-link"> Login </a>
			</li>
		`
	registerLink.innerHTML = 
		`
			<li class="nav-item ">
				 <a href="./pages/register.html" class="nav-link"> Register </a>
			</li>
		`	
} 
else {
	// if (!isAdmin) {

	navItems.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./pages/profilePage.html" class="nav-link"> Profile Page </a>
			</li>
		`
	registerLink.innerHTML = 
		`
			<li class="nav-item ">
				 <a href="./pages/logout.html" class="nav-link"> Log Out </a>
			</li>
		`	
}
	// }
	// else {
	// 	navItems.innerHTML = 
	// 	`
	// 		<li class="nav-item ">
	// 			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
	// 		</li>
	// 	`
	// }
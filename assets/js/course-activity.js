//ACTIVITY

let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")

//if statement that will show a button for adding courses or redirecting to addCourse page for admin users; however, if regular user, button will not appear

if (adminUser === false || adminUser === null) {
	addButton.innerHTML = null
} 
else {
	addButton.innerHTML = `

		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Courses</a>
		</div>

	`
}
//ACTIVITY
fetch('https://aqueous-garden-97949.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	console.log(data)
})

let registerForm = document.querySelector("#registerUser");
let inputs = Array.from(document.querySelectorAll("input"))
let submitBtn = document.querySelector("#submitBtn")
// console.log(inputs)

inputs[0].addEventListener("keydown", () => {
	checkInput()
})
inputs[1].addEventListener("keydown", () => {
	checkInput()
})
inputs[2].addEventListener("keydown", () => {
	checkInput()
})
inputs[3].addEventListener("keydown", () => {
	checkInput()
})
inputs[4].addEventListener("keydown", () => {
	checkInput()
})
inputs[5].addEventListener("keydown", () => {
	checkInput()
})

function checkInput() {
	if (inputs[0].value.length === 0 || inputs[1].value.length === 0 || 
		inputs[2].value.length === 0 || inputs[3].value.length === 0 || 
		inputs[4].value.length === 0 || inputs[5].value.length === 0 ) 
		{
		submitBtn.disabled = true
	} 
	else {
		submitBtn.disabled = false
	}
}


registerForm.addEventListener("submit", (e) => {
	e.preventDefault()
	// console.log("I triggered the submit event")
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value
	
	if ((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {
		// console.log("password1 and password2 are not blank and matches each other")

		//fetch (url, options) options >> object
		fetch('https://aqueous-garden-97949.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if (data === false) {
				fetch('https://aqueous-garden-97949.herokuapp.com/api/users', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json'},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				}) 
				.then(res => res.json())
				.then(data => {
					// console.log(data);

					if(data === true) {
						alert("Registered Successfully")
						//redirect to login page
						window.location.replace("./login.html")
					} else {
						//error in creating registration
						alert("Something went wrong")
					}

				})
			} else {
				alert("Email already exists")
			}
			
		})
	} else {
		alert("Retry password verification and input 11-Digit Mobile Number")
	}
})
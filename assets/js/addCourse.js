let formSubmit = document.querySelector("#createCourse")
// console.log(formSubmit)
let submitBtn = document.querySelector("#submitBtn")
let inputs = Array.from(document.querySelectorAll('input'))

inputs[0].addEventListener("keydown", () => {
	checkInput()
})
inputs[1].addEventListener("keydown", () => {
	checkInput()
})
inputs[2].addEventListener("keydown", () => {
	checkInput()
})

function checkInput() {
	if (inputs[0].value.length === 0 || inputs[1].value.length === 0 || inputs[2].value.length === 0) {
		submitBtn.disabled = true
	} else {
		submitBtn.disabled = false
	}
}

//add an event listener:
formSubmit.addEventListener("submit", (e) => {
	// 	Q: What does preventDefault( ) do?
	// A: it prevents the normal behavior of an event. In this case, the event is submit and its default behavior is to refresh the page when submitting the form.

	e.preventDefault()

	//get the values of your input:
	let courseName = document.querySelector("#courseName").value
	let price = document.querySelector("#coursePrice").value
	let description = document.querySelector("#courseDescription").value

	// check(courseName,price,description);
	// COMMON ERRORS: 
	// Cannot read property "values" of null
	// 	this is the object --> example: object.Name
	// Cannot read property name of null 
	// 	object is null 

	//Get the JWT from our localStorage
	let token = localStorage.getItem("token")
	// console.log(token)

	//Create a fetch request to add a new course:
	fetch("https://aqueous-garden-97949.herokuapp.com/api/courses", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {
		//if the creation of the course is successful, redirect admin to the courses page
		if (data === true) {
			window.location.replace('./courses.html')
		} else {
			//Errir while creating a course:
			alert("Course Creation Failed: Something Went Wrong")
		}
	})
})

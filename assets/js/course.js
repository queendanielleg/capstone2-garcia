
let params = new URLSearchParams(window.location.search)

//the has method for URLSearchParams checks if the courseId key exists in our URL Query string
// console.log(params.has('courseId'))

//the get method for URLSearchParams returns the value of the key, passed in as an argument
// console.log(params.get('courseId'))

// store the courseId from the URL Query string in a variable:
let courseId = params.get('courseId')
//get the token from the localStorage
let token = localStorage.getItem("token")
let fromUserId = localStorage.getItem("id")
// console.log(token)
// console.log(fromUserId)

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

//not defined vs undefined 
// undefined = variable was declared but no initial value
// not defined = variable does not exist

//get the details of a single course
fetch(`https://aqueous-garden-97949.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	// console.log(data)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="mb-5 btn btn-block courseBtn">Enroll</button>`

	document.querySelector("#enrollButton").addEventListener("click", () => {
		// add fetch request to enroll user
		fetch('https://aqueous-garden-97949.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data === true) {
				alert("Thank you for enrolling to the course")
				window.location.replace('./courses.html')
			}
			else {
				//server error while enrolling to the course
				alert("Something went wrong")
			}
		})
	})

	let enrollees = [];
	enrollees = data.enrollees
	// console.log(enrollees)

	enrollees.forEach( data => {
		// console.log(data.userId)
		if( fromUserId === data.userId) {
			enrollContainer.innerHTML = `<button id="enrollButton" class="mb-5 btn btn-block courseBtn disabled">Already Enrolled</button>`
			document.querySelector("#enrollButton").addEventListener("click", () => {
				alert("You already enrolled to this course")
				window.location.replace('./courses.html')
			})
		}
	})
})
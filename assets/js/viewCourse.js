// console.log("hello from viewCourse")

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
// console.log(courseId)

let token = localStorage.getItem("token")
// console.log(token)

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")

let enrolleeName = document.querySelector("#enrolleeName")

fetch(`https://aqueous-garden-97949.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then( data => {
	// console.log(data)
	// courseName.innerHTML = data.
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	// userId = data.enrollees[0].userId

	enrollees = data.enrollees
	// console.log(enrollees)
	let listEnrollees=[];

	enrollees.map( data => {
		// console.log(data)
		let userId = data.userId
		fetch(`https://aqueous-garden-97949.herokuapp.com/api/users/${userId}`)
		.then(res => res.json())
		.then( data => {
			name = `${data.firstName} ${data.lastName}`
			// console.log(name)
			listEnrollees.push(name)
			listEnrollees.sort()
			// console.log(listEnrollees)
		}).then( data => {
			let list = listEnrollees.map(data => {
				return (`
					<div class="col-md-5 forList mb-4"> <img src="./../assets/images/backpack.png" alt="Tent" class="img-fluid rounded-circle"> Name: ${data}</div>`)
			}).join("")
			enrolleeName.innerHTML = list;
		})
	})
})
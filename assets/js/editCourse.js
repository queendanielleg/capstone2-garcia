// console.log("hello")

let submitBtn = document.querySelector("#submitBtn")
let inputs = Array.from(document.querySelectorAll('input'))

inputs[0].addEventListener("keydown", () => {
	checkInput()
})
inputs[1].addEventListener("keydown", () => {
	checkInput()
})
inputs[2].addEventListener("keydown", () => {
	checkInput()
})

function checkInput() {
	if (inputs[0].value.length === 0 || inputs[1].value.length === 0 || inputs[2].value.length === 0) {
		submitBtn.disabled = true
	} else {
		submitBtn.disabled = false
	}
}

let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
// console.log(courseId);
let token = localStorage.getItem('token');

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")

fetch(`https://aqueous-garden-97949.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then( data => {
	// console.log(data)
	courseName.placeholder = data.name
	courseDesc.placeholder = data.description
	coursePrice.placeholder = data.price

	courseName.value = data.name
	courseDesc.value = data.description
	coursePrice.value = data.price

	let formSubmit = document.querySelector("#editCourse")

		formSubmit.addEventListener("submit", (e) => {

		e.preventDefault()

		let name= courseName.value
		let price = coursePrice.value
		let description = courseDesc.value

		// console.log(name)
		// console.log(price)
		// console.log(description)

		fetch("https://aqueous-garden-97949.herokuapp.com/api/courses", {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				id: courseId,
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("Course Edit Failed: Something Went Wrong")
			}
		})
	})	
})

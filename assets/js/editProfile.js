// console.log("hello from editProfile")

let userId = localStorage.getItem("id")
let token = localStorage.getItem("token")

let submitBtn = document.querySelector("#submitBtn")
let inputs = Array.from(document.querySelectorAll('input'))

// console.log(inputs)

inputs[0].addEventListener("keydown", () => {
	checkInput()
})
inputs[1].addEventListener("keydown", () => {
	checkInput()
})
inputs[2].addEventListener("keydown", () => {
	checkInput()
})


function checkInput() {
	if (inputs[0].value.length === 0 || inputs[1].value.length === 0 || inputs[2].value.length === 0 ) {
		submitBtn.disabled = true
	} else {
		submitBtn.disabled = false
	}
}

let firstName = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let mobileNo = document.querySelector("#mobileNumber")

fetch(`https://aqueous-garden-97949.herokuapp.com/api/users/${userId}`)
.then( res => res.json())
.then( data => {
	// console.log(data)

	firstName.value = data.firstName
	lastName.value = data.lastName
	mobileNo.value = data.mobileNo

	let editProfile = document.querySelector("#editProfile")

	editProfile.addEventListener( "submit", (e) => {
		e.preventDefault()

		let fname = firstName.value
		let lname = lastName.value
		let newMobileNo = mobileNo.value
		
		if (newMobileNo.length === 11) {
			fetch("https://aqueous-garden-97949.herokuapp.com/api/users", {
				method: "PUT",
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					id: userId,
					firstName: fname,
					lastName: lname,
					mobileNo: newMobileNo
				})
			})
			.then( res => res.json())
			.then( data => {
				alert("You have successfully edited your profile")
				window.location.replace("./profilePage.html")
			})	
		}
		else {
			alert("Mobile Number has to have 11 digits")
		}	
	})
})
